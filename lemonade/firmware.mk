LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),lemonade)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
abl \
cpucp \
dtbo \
hyp \
modem \
oplusstanvbk \
qweslicstore \
tz \
xbl \
aop \
devcfg \
engineering_cdt \
imagefv \
multiimgoem \
shrm \
uefisecapp \
xbl_config \
bluetooth \
dsp \
featenabler \
keymaster \
oplus_sec \
qupfw \
splash \
vm-bootsys

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif

